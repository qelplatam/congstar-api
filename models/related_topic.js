const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('related_topic', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    section_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'section',
        key: 'id'
      }
    },
    related_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'section',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'related_topics',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "section_id",
        using: "BTREE",
        fields: [
          { name: "section_id" },
        ]
      },
      {
        name: "related_id",
        using: "BTREE",
        fields: [
          { name: "related_id" },
        ]
      },
    ]
  });
};
