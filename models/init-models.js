var DataTypes = require("sequelize").DataTypes;
var _bill = require("./bill");
var _page_bill = require("./page_bill");
var _page = require("./page");
var _related_topic = require("./related_topic");
var _section = require("./section");

function initModels(sequelize) {
  var bill = _bill(sequelize, DataTypes);
  var page_bill = _page_bill(sequelize, DataTypes);
  var page = _page(sequelize, DataTypes);
  var related_topic = _related_topic(sequelize, DataTypes);
  var section = _section(sequelize, DataTypes);

  page_bill.belongsTo(bill, { as: "bull", foreignKey: "bull_id"});
  bill.hasMany(page_bill, { as: "page_bills", foreignKey: "bull_id"});
  page_bill.belongsTo(page, { as: "pages", foreignKey: "page_id"});
  page.hasMany(page_bill, { as: "page_bills", foreignKey: "page_id"});
  section.belongsTo(page, { as: "pages", foreignKey: "page_id"});
  page.hasMany(section, { as: "sections", foreignKey: "page_id"});
  related_topic.belongsTo(section, { as: "section", foreignKey: "section_id"});
  section.hasMany(related_topic, { as: "related_topics", foreignKey: "section_id"});
  related_topic.belongsTo(section, { as: "related", foreignKey: "related_id"});
  section.hasMany(related_topic, { as: "related_related_topics", foreignKey: "related_id"});

  return {
    bill,
    page_bill,
    page,
    related_topic,
    section,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
